```
kubectl label nodes <your-node-name> nodetype=scheduler
kubectl label nodes <your-node-name> nodetype=worker
kubectl label nodes <your-node-name> nodetype=worker
kubectl create -f service.yaml
kubectl create -f service_internal.yaml

kubectl create -f scheduler.yaml
kubectl create -f deployment.yaml
```