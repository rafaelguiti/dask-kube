#!/bin/bash
kubectl create -f scheduler.yaml
kubectl create -f service.yaml
kubectl create -f service_internal.yaml
kubectl create -f deployment.yaml
